const { Schema, model } = require('mongoose')
const productSchema = new Schema({
    name: { type: String, required: true },
    date: { type: Date, default: Date.now },
    averageRating: { type: Number, default: 0 },
    description: { type: String, default: Date.now },
    comments: [{
        content: { type: String, required: true },
        rating: Number
    }]


})

module.exports = model('product', productSchema)