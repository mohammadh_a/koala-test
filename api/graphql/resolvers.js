const { loremIpsum } = require("lorem-ipsum");
const Product = require('../models/Product')


module.exports = {
    Query: {
        products: async () => {
            const product = await Product.find()
            return product
        },
    },
    Mutation: {
        createProduct: async (_, { productInput: input }) => {
            const product = new Product({
                name: input.name,
                description: loremIpsum()
            })
            await product.save()
            return product
        },
        addComment: async (_, { productId, commentInput: input }) => {
            const comment = { content: input.content, rating: input.rating }
            const product = await Product.findById(productId)
            product.comments.push(comment)
            let sum = 0
            product.comments.forEach(element => {
                sum += element.rating
            })
            product.averageRating = sum / product.comments.length;
            await product.save()
            return product
        }
    }
}