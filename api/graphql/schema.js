const { gql } = require('apollo-server-express');

module.exports = gql `type Product {
  _id: ID!
  name: String!
  averageRating: Float!
  description: String!
  date: String!
  comments: [Comment!]!
}
type Comment {
  content: String!
  rating: Float
}
input ProductInput {
  name: String!
}
input CommentInput {
  content: String!
  rating: Float
}
type Query {
  products: [Product!]!
}
type Mutation {
  createProduct(productInput: ProductInput): Product!
  addComment(productId: ID!, commentInput: CommentInput): Product!
}
`