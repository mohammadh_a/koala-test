const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const morgan = require('morgan')
const { ApolloServer } = require('apollo-server-express');
const gqlSchema = require('./api/graphql/schema')
const gqlResolvers = require('./api/graphql/resolvers')
const cors = require('cors')

app.use(bodyParser.json())
app.use(morgan('dev'));
app.use(cors());

mongoose.connect('mongodb://localhost:27017/Koala', {
    useNewUrlParser: true
});

const typeDefs = gqlSchema
const resolvers = gqlResolvers
const server = new ApolloServer({
    typeDefs,
    resolvers,
});

server.applyMiddleware({ app });

const PORT = 3000
app.listen(PORT, () => {
    console.log(`listening on ${PORT}...`);

})