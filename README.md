This project is a test on how to query in graphQl and is CORS enabled.

Steps on how to install:
1. Clone the project
    
1. Install dependencies using `npm install`
    
1. run server (in runs at port 3000 by default) by `node index` or `nodemon index`
    database : name : Koala , port: 27017 (default)
    
1. You can query it in Graphiql at `http://localhost:3000/graphql` or in front-end
    
    
Available Queries:

Query: 
    
    - products
    
Mutation:
    
    createProduct(name)
    
    addComment(_id,input)
    
for more info about queries press ctrl + space in playground or visit `api/graphql/schema.js`



query example:
```
query{
  products{
    name
    averageRating
    comments{
      rating
    }
  }
}
```
```
mutation{
    createProduct(productInput:{name:"Galaxy Note 10"}){
        _id
        name
        averageRating
        description
        date
      }
    }
}
```
```
mutation{
     addComment(productId:"5d68e281bb75b1127031f695",commentInput:{content:"its good",rating:5}){
      name
      averageRating
      comments{
        content
        rating
      }
    }
}
```